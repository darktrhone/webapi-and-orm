﻿using Autofac;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Autofac;
using Nancy.CustomErrors;
using Nancy.Extensions;
using Nancy.Serialization.JsonNet;
using Service.Infrastructure.Services;
using Common.Infrastructure.Services.SubscriptionsRepositorie;
using Service.CofinaPaymentsService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Service.CofinaSubscriptionsService;
using Service.Infrastructure.Services.Subscriptions;
using Nancy.Authentication.Stateless;
using Common.Infrastructure.Services;

namespace Service
{
    public class ServiceBootstrapper : AutofacNancyBootstrapper
    {
        public ServiceBootstrapper()
        {

        }


        /// <summary>
        ///     Gets the application level container
        /// </summary>
        /// <returns>
        ///     Container instance
        /// </returns>
        protected override ILifetimeScope GetApplicationContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SubscriptionsServiceHandler>().As<ISubscriptionsServiceHandler>();
            builder.RegisterType<SubscriptionsRepositorie>().As<ISubscriptionsRepositorie>();
            builder.RegisterType<UsersRepositorie>().As<IUsersRepositorie>();
            builder.RegisterType<PaymentsServiceClient>();
            builder.RegisterType<SubscriptionsServiceClient>();
            builder.RegisterType<SubscriptionsServiceClient>();
            builder.RegisterType<CreateSubscriptions>().As<ICreateSubscriptions>();

            return builder.Build();
        }

        /// <summary>
        ///     Initialise the bootstrapper - can be used for adding pre/post hooks and
        ///     any other initialisation tasks that aren't specifically container setup
        ///     related.
        /// </summary>
        /// <param name="container">Container instance for resolving types if required.</param>
        /// <param name="pipelines">The pipelines.</param>
        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            StaticConfiguration.EnableRequestTracing = true;

            CustomErrors.Enable(
                pipelines,
                new DefaultErrorConfiguration(/*ex => Log.Error(ex.Message, ex)*/),
                new JsonNetSerializer());
        }

        /// <summary>
        ///     Initialise the request - can be used for adding pre/post hooks,
        ///     and any other per-request initialisation tasks that aren't specifically container setup related
        ///     This will log all requests and responses.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="pipelines">The pipelines.</param>
        /// <param name="context">The context.</param>
        protected override void RequestStartup(ILifetimeScope container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            // At request startup we modify the request pipelines to
            // include stateless authentication
            //
            // Configuring stateless authentication is simple. Just use the
            // NancyContext to get the apiKey. Then, use the apiKey to get
            // your user's identity.
            var configuration =
                new StatelessAuthenticationConfiguration(nancyContext =>
                {
                    //for now, we will pull the apiKey from the querystring,
                    //but you can pull it from any part of the NancyContext
                    var token = nancyContext.Request.Headers.Keys.Contains("Token") ? nancyContext.Request.Headers["Token"].FirstOrDefault() : "";

                    var authentication = Auth.ValidateToken(token);
                    //var apiKey = (string)nancyContext.Request.Headers().Query.ApiKey.Value;
                    
                    return Auth.ValidateToken(token);

                });


            AllowAccessToConsumingSite(pipelines);

            StatelessAuthentication.Enable(pipelines, configuration);


            pipelines.BeforeRequest += ctx =>
            {
                var client = ctx.Request.Headers.Keys.Contains("Client") ? ctx.Request.Headers["Client"].First() : "Unknown";

                var request = new
                {
                    Operation = ctx.Request.Path,
                    Client = client,
                    ClientIp = ctx.Request.UserHostAddress
                };
                //Log.Debug("Request received {@request}", request);
                return null; // This means that the request pipeline will not be interrupted at this point
            };

            //pipelines.AfterRequest += ctx => Log.Debug("Returning with response for {0}", ctx.Request.Path);
        }

        static void AllowAccessToConsumingSite(IPipelines pipelines)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline(x =>
            {
                x.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                x.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT,OPTIONS");
            });
        }

    }

    public class DefaultErrorConfiguration : CustomErrorsConfiguration
    {
        private readonly IDictionary<Type, HttpStatusCode> errorCodesMapping = new Dictionary<Type, HttpStatusCode>
            {
                { typeof(ValidationException), HttpStatusCode.BadRequest },
                { typeof(ArgumentException), HttpStatusCode.BadRequest },
                { typeof(ArgumentNullException), HttpStatusCode.BadRequest },
                { typeof(ArgumentOutOfRangeException), HttpStatusCode.BadRequest },
                { typeof(InvalidEnumArgumentException), HttpStatusCode.BadRequest },
                { typeof(UnauthorizedAccessException), HttpStatusCode.Unauthorized }
            };

        public DefaultErrorConfiguration()
        {
            Debug = true;

            // Map error status codes to custom view names
            ErrorViews[HttpStatusCode.NotFound] = "error";
            ErrorViews[HttpStatusCode.InternalServerError] = "error";
            ErrorViews[HttpStatusCode.Forbidden] = "error";
            ErrorViews[HttpStatusCode.BadRequest] = "error";
            ErrorViews[HttpStatusCode.Unauthorized] = "error";
        }

        public DefaultErrorConfiguration(Action<Exception> logFunc) : this()
        {
            this.LogError = logFunc;
        }

        public Action<Exception> LogError { get; set; }

        /// <summary>
        ///     Handles the error.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="ex">The exception.</param>
        /// <param name="serializer">The serializer.</param>
        /// <returns>An ErrorResponse.</returns>
        /// <remarks>
        ///     Custom mapping of a thrown exception to an ErrorResponse with status code
        ///     The implementation in this example is the default implementation used in
        ///     Nancy.CustomErrors.CustomErrorConfiguration. Override this if you need to
        ///     Map custom exception types to different status codes, or error objects.
        ///     An example might be to map a custom security exception to HttpForbidden status
        ///     code, rather than the default InternalServerError status code
        /// </remarks>
        public override ErrorResponse HandleError(NancyContext context, Exception ex, ISerializer serializer)
        {
            context.WriteTraceLog(builder => builder.AppendLine(ex.Message));

            if (this.LogError != null)
                this.LogError(ex);

            var error = new Error
            {
                FullException = ex.ToString(),
                Message = ex.Message
            };

            var statusCode = HttpStatusCode.InternalServerError;

            // Map exception types to HttpStatusCodes
            var exceptionType = ex.GetType();
            if (this.errorCodesMapping.Keys.Contains(exceptionType))
            {
                statusCode = this.errorCodesMapping[exceptionType];
            }

            return new ErrorResponse(error, serializer)
                .WithStatusCode(statusCode) as ErrorResponse;
        }
    }

}