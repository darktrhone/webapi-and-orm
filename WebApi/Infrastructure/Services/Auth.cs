﻿using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Common.Infrastructure.Services.Security;

namespace Service.Infrastructure.Services
{
    public class Auth : IUserIdentity
    {
        public IEnumerable<string> Claims
        {
            get;
            private set;
        }

        public string UserName
        {
            get;
            private set;
        }

        public static IUserIdentity ValidateToken(string token)
        {
            SecurityManager securityManager = new SecurityManager();
            
            if (securityManager.IsTokenValid(token))
            {
                var claims = new List<string>();
                claims.Add("full");
                return new Auth
                {
                    Claims = claims,
                    UserName = "cofina"

                };
            }

            return null;
        }
    }
}