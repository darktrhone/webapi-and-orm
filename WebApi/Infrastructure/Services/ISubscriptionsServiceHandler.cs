﻿using Service.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Service.Infrastructure.Services
{
    public interface ISubscriptionsServiceHandler
    {
        Task<List<string>> List();
        Task<PostSubscribeResponse> Add(PostSubscribeRequest request);
    }
}