﻿using Service.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Infrastructure.Services.Subscriptions
{
    public interface ICreateSubscriptions
    {
        Task<PostSubscribeResponse> Create(PostSubscribeRequest request);
    }
}
