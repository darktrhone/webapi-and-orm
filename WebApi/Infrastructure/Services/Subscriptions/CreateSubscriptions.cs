﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Service.Infrastructure.Domain;
using Common.Infrastructure.Services.SubscriptionsRepositorie;
using Service.CofinaPaymentsService;
using Service.CofinaSubscriptionsService;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Cofina.Logger;
using Common.Infrastructure.Services;

namespace Service.Infrastructure.Services.Subscriptions
{
    public class CreateSubscriptions : ICreateSubscriptions
    {
        public ISubscriptionsRepositorie Repositorie { get; set; }
        public IUsersRepositorie UsersRepositorie { get; set; }
        private PaymentsServiceClient PaymentClient { get; set; }
        public SubscriptionsServiceClient CofinaSubscriptionService { get; set; }

        public CreateSubscriptions(ISubscriptionsRepositorie repositorie, IUsersRepositorie usersRepositorie,
            PaymentsServiceClient paymentClient, SubscriptionsServiceClient subscriptionClient)
        {
            Repositorie = repositorie;
            UsersRepositorie = usersRepositorie;
            PaymentClient = paymentClient;
            CofinaSubscriptionService = subscriptionClient;
        }

        public async Task<PostSubscribeResponse> Create(PostSubscribeRequest request)
        {

            try
            {
                
                // Validar se já está pago
                if (request.PaymentId != null)
                {

                    var payment = await PaymentClient.GetPaymentAsync((int)request.PaymentId).ConfigureAwait(false);

                    if (payment.State != PaymentState.Payed)
                    {
                        HttpContext.Current.Response.AddHeader("COF-Message", "Estado da assinatura diferente de pago");
                        return null;
                    }
                }

                // Se não tiver id de pagamento, é obrigado a ter um código de promoção
                if (request.PaymentId == null && string.IsNullOrEmpty(request.PromotionalCode))
                {
                    HttpContext.Current.Response.AddHeader("COF-Message", "Falta codigo de oferta ou ID de pagamento valido");
                    return null;
                }

                CodePromotion codepromo = null;

                if (!string.IsNullOrEmpty(request.PromotionalCode))
                {
                    codepromo = GetCode(request.PromotionalCode, request.PublicationId);
                }

                // Se não tiver um código de promoção, sem id de pagamento, o código tem que ser do tipo oferta
                if (!string.IsNullOrEmpty(request.PromotionalCode) && request.PaymentId == null)
                {

                    if (codepromo != null)
                    {
                        if(codepromo.State != CodePromotionState.Waiting)
                        {
                            HttpContext.Current.Response.AddHeader("COF-Message", "Codigo invalido");
                            return null;
                        }
                            
                        if (codepromo.Type != CodePromotionType.Oferta && codepromo.Type != CodePromotionType.Universal)
                        {
                            HttpContext.Current.Response.AddHeader("COF-Message", "Precisa de um Id de pagamento valido");
                            return null;
                        }
                    }

                }

                var User = UsersRepositorie.GetUserById((int)request.UserId);

                CofinaSubscriptionsService.SSOUser SubscriptionUser = new CofinaSubscriptionsService.SSOUser
                {
                    UserID = User.UserID,
                    Email = User.Email,
                    Name = User.Name,
                    Surname = User.Surname
                };

                var sub = Repositorie.GetSubscription(request.PublicationId, request.SubscriptionId);

                string initialDate = DateTime.Now.ToString("yyyy-MM-dd");
                int duration = sub.DurationInDays;
                int TypeSubscription = sub.SubscriptionTypeID;
                string endDate = DateTime.Now.AddDays((double)duration).ToString("yyyy-MM-dd");

                CofinaSubscriptionsService.Subscription subscription = new CofinaSubscriptionsService.Subscription();
                subscription.SignerLogin = User.Email;
                subscription.Dte_Init = DateTime.Parse(initialDate);
                subscription.Dte_End = DateTime.Parse(endDate);
                subscription.PublicationID = request.PublicationId;
                subscription.SignerID = User.UserID;
                subscription.RenewDate = DateTime.Parse(endDate);
                subscription.UnicreWallet = "0" + request.PublicationId + User.UserID.ToString();
                subscription.IsSelfRenewable = sub.SelfRenewable;


                if (codepromo != null && codepromo.CodePromotionID > 0)
                {
                    subscription.CodePromotionID = codepromo.CodePromotionID;
                }

                if (request.PaymentId != null)
                {
                    subscription.PaymentID = (int)request.PaymentId;
                }

                if (TypeSubscription > 0)
                    subscription.TypeID = TypeSubscription;
                else
                    subscription.TypeID = sub.SubscriptionTypeID;

                subscription = CofinaSubscriptionService.CreateSubscription(subscription);

                if (subscription.State == CofinaSubscriptionsService.SubcriptionState.Activa)
                {

                    if (codepromo != null)
                    {

                        codepromo.SubscripionID = subscription.SubscriptionID;

                        CofinaSubscriptionService.UseCode(codepromo, SubscriptionUser);

                    }
                }


                PostSubscribeResponse response = new PostSubscribeResponse()
                {
                    CreatedSubscriptionId = subscription.SubscriptionID,
                    SubscriptionId = request.SubscriptionId,
                    StartDate = subscription.Dte_Init,
                    EndDate = subscription.Dte_End,
                    UserId = SubscriptionUser.UserID,
                    Email = SubscriptionUser.Email,
                    Name = SubscriptionUser.Name
                };

                CallUrlCallBack(response, request.CallBackUrl);

                return response;
            }
            catch (Exception error)
            {
                CofinaLogList.Instance.Add(error);
            }
            finally
            {
                CofinaLogList.Instance.Log();
            }

            return null;
        }

        private CodePromotion GetCode(string code, int publicationId)
        {
            CodePromotion codepromo = new CodePromotion();

            codepromo.Code = code;
            codepromo.PublicationID = publicationId;

            SubscriptionsServiceClient processcodepromotion = new SubscriptionsServiceClient();
            codepromo = processcodepromotion.CheckCode(codepromo);

            return codepromo;
        }

        public void CallUrlCallBack(PostSubscribeResponse postResponse, string callBackUrl)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(callBackUrl))
                {
                    var responseService = CallSubscriptionConfirmation(postResponse, callBackUrl);
                }
            }
            catch (Exception error)
            {
                CofinaLogList.Instance.Add(error);
            }
            finally
            {
                CofinaLogList.Instance.Log();
            }

        }

        public HttpClient client = new HttpClient();
        public async Task<CallBackSubscribeResponse> CallSubscriptionConfirmation(PostSubscribeResponse postResponse, string callBackUrl)
        {
            client.BaseAddress = new Uri(callBackUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.PostAsJsonAsync("", postResponse);
            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsAsync<CallBackSubscribeResponse>();

            return contentResponse;
        }

    }
}