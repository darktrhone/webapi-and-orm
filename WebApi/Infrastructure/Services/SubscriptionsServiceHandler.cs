﻿using Common.Infrastructure.Services.SubscriptionsRepositorie;
using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Security;
using Service.CofinaPaymentsService;
using Service.CofinaSubscriptionsService;
using Service.Infrastructure.Domain;
using Service.Infrastructure.Services.Subscriptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Infrastructure.Services
{
    public class SubscriptionsServiceHandler : ISubscriptionsServiceHandler
    {
        public ICreateSubscriptions CreateSubscription { get; set; }
        public SubscriptionsServiceHandler(ICreateSubscriptions createSubscription)
        {
            CreateSubscription = createSubscription;
        }

        public async Task<List<string>> List()
        {
            return new List<string> { "1", "2" };
        }

        public async Task<PostSubscribeResponse> Add(PostSubscribeRequest request)
        {

            if (request.SubscriptionId != 0 && request.UserId != null)
            {

                PostSubscribeResponse subscription = await CreateSubscription.Create(request);
                
                return subscription;

            }

            return new PostSubscribeResponse() { CreatedSubscriptionId = 0 };
        }
    }
}