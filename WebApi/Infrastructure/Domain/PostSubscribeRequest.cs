﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Infrastructure.Domain
{
    public class PostSubscribeRequest
    {
        public int PublicationId { get; set; }
        public int SubscriptionId { get; set; }
        public int? PaymentId { get; set; }
        public string PromotionalCode { get; set; }
        public int? UserId { get; set; }
        public string CallBackUrl { get; set; }
    }
}