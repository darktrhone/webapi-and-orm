﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Infrastructure.Domain
{
    public class PostSubscribeResponse
    {
        public int CreatedSubscriptionId { get; set; }
        public int SubscriptionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }

    }
}