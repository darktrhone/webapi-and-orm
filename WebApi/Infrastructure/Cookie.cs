﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Assinaturas.Site.Infrastructure.Services
{
    public static class Cookie
    {
        public static void SaveCookieWithRawValue(string name, string value, TimeSpan absoluteExpire)
        {
            HttpCookie cookie = new HttpCookie(name);

            cookie.Value = value;
            cookie.Expires = DateTime.Now.Add(absoluteExpire);

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Domain"]))
                cookie.Domain = ConfigurationManager.AppSettings["Cookies_Domain"];

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Path"]))
                cookie.Path = ConfigurationManager.AppSettings["Cookies_Path"];

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void SaveCookie(string name, string value, TimeSpan absoluteExpire, bool encript = true)
        {
            HttpCookie cookie = new HttpCookie(name);

            cookie.Value = encript ? EncodeTo64(value) : HttpContext.Current.Server.UrlEncode(value);
            cookie.Expires = DateTime.Now.Add(absoluteExpire);

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Domain"]))
                cookie.Domain = ConfigurationManager.AppSettings["Cookies_Domain"];

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Path"]))
                cookie.Path = ConfigurationManager.AppSettings["Cookies_Path"];

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void SaveObjectCookie(string name, object value, TimeSpan absoluteExpire)
        {
            if (value == null)
                return;


            HttpCookie cookie = new HttpCookie(name);
            cookie.Value = EncodeTo64(value);
            cookie.Expires = DateTime.Now.Add(absoluteExpire);

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Domain"]))
                cookie.Domain = ConfigurationManager.AppSettings["Cookies_Domain"];

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Path"]))
                cookie.Path = ConfigurationManager.AppSettings["Cookies_Path"];

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static T GetObjectCookie<T>(string name)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];

            if (cookie == null)
                return default(T);

            string cookieValue = cookie.Value;

            return (T)DecodeFrom64<T>(cookieValue);
        }

        public static string GetCookie(string name, bool isEncripted = true)
        {
            try
            {

                HttpCookie cookie = HttpContext.Current.Request.Cookies[name];

                if (cookie == null)
                    return null;

                string cookieValue = cookie.Value;

                if (string.IsNullOrWhiteSpace(cookieValue) || !isEncripted)
                    return HttpContext.Current.Server.UrlDecode(cookieValue);

                return DecodeFrom64<string>(cookieValue);

            }
            catch (Exception e)
            { 
                try
                {
                    RemoveCookie(name);
                }
                catch
                { }

                return null;
            }
        }

        public static void RemoveCookie(string name)
        {
            HttpCookie cookie = new HttpCookie(name);

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Domain"]))
                cookie.Domain = ConfigurationManager.AppSettings["Cookies_Domain"];

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Cookies_Path"]))
                cookie.Path = ConfigurationManager.AppSettings["Cookies_Path"];

            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);

            HttpCookie noDomainCookie = new HttpCookie(name);
            noDomainCookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(noDomainCookie);
        }

        private static string EncodeTo64(object toEncode)
        {
            string sToEncode = SerializeToString(toEncode);

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sToEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        private static T DecodeFrom64<T>(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return DeserializeFromString<T>(returnValue);
        }

        private static string SerializeToString(object obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }

        private static T DeserializeFromString<T>(string serializedObject)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                using (StringReader reader = new StringReader(serializedObject))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            { 
                return (T)(object)serializedObject;
            }

        }
    }
}