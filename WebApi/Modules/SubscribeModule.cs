﻿using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Metadata.Modules;
using Nancy.Responses.Negotiation;
using Nancy.Security;
using Nancy.Swagger;
using Service.Infrastructure.Domain;
using Service.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Service.Modules
{

    public class SubscribeMetadataModule : MetadataModule<SwaggerRouteData>
    {
        public SubscribeMetadataModule()
        {
            this.Describe["subscribeList"] = description => description.AsSwagger(
                with =>
                {
                    with.ApiPath("/subscribe/list");
                    with.ResourcePath("/subscribe/list");
                    with.Summary("This operation ....");
                    with.Produces(new MediaType("application/json"));
                    with.Response((int)HttpStatusCode.BadRequest);
                    with.Response((int)HttpStatusCode.InternalServerError);
                    with.Response((int)HttpStatusCode.OK);
                });

            this.Describe["subscribeAdd"] = description => description.AsSwagger(
                with =>
                {
                    with.ApiPath("/subscribe");
                    with.ResourcePath("/subscribe");
                    with.Summary("This operation ....");
                    with.Model<PostSubscribeRequest>();
                    with.BodyParam<PostSubscribeRequest>("A Subscribe object", required: true);
                    with.Produces(new MediaType("application/json"));
                    with.Consumes(new MediaType("application/json"));
                    with.Response((int)HttpStatusCode.BadRequest);
                    with.Response((int)HttpStatusCode.InternalServerError);
                    with.Response((int)HttpStatusCode.OK);
                });
        }
    }
    
    public class SubscribeModule : BaseNancyModule
    {
        public SubscribeModule(ISubscriptionsServiceHandler subscriptionsServiceHandler)
        {

            this.GetAsync<List<string>>("subscribeList", "/subscribe/list", subscriptionsServiceHandler.List);

            this.PostAsync<PostSubscribeRequest, PostSubscribeResponse>("subscribeAdd", "/subscribe", subscriptionsServiceHandler.Add);
        }
    }
}