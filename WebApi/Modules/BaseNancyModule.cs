﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Service.Modules
{
    public abstract class BaseNancyModule : NancyModule
    {
        
        public void GetAsync<TResponse>(string name, string route, Func<Task<TResponse>> serviceFunc, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Get[name, route, true] = async (parameters, cancellationToken) =>
            {
                //Log.Debug("Request: {@Request}", request);

                var response = await serviceFunc();

                return this.Negotiate
                    .WithStatusCode(statusCode)
                    .WithModel(response);
            };
        }

        public void GetAsync<TRequest, TResponse>(string name, string route, Func<TRequest, Task<TResponse>> serviceFunc, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Get[name, route, true] = async (parameters, cancellationToken) =>
            {
                var request = this.Bind<TRequest>();
                //Log.Debug("Request: {@Request}", request);

                var response = await serviceFunc(request);

                return this.Negotiate
                    .WithStatusCode(statusCode)
                    .WithModel(response);
            };
        }


        public void PostAsync<TRequest, TResponse>(string name, string route, Func<TRequest, Task<TResponse>> serviceFunc, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Post[name, route, true] = async (parameters, cancellationToken) =>
            {
                this.RequiresAuthentication();

                if (this.Context.CurrentUser == null)
                {
                    return this.Negotiate
                        .WithStatusCode(401)
                        .WithHeader("x-Reason", "Invalid Token");
                }

                if (!this.Context.CurrentUser.HasClaim("full"))
                {
                    return this.Negotiate
                        .WithStatusCode(401)
                        .WithHeader("x-Reason", "Invalid Token");                    
                }

                var request = this.Bind<TRequest>();
                //Log.Debug("Request: {@Request}", request);

                var response = await serviceFunc(request);

                return this.Negotiate
                    .WithStatusCode(statusCode)
                    .WithModel(response);
            };
        }

        public void PostAsync<TRequest>(string name, string route, Func<TRequest, Task> serviceFunc, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Post[name, route, true] = async (parameters, cancellationToken) =>
            {
                var request = this.Bind<TRequest>();
                //Log.Debug("Request: {@Request}", request);

                await serviceFunc(request);

                return this.Negotiate
                    .WithStatusCode(statusCode);
            };
        }
    }
}