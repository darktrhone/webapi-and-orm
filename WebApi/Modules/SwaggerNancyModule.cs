﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Modules
{
    public class SwaggerNancyModule : NancyModule
    {
        public SwaggerNancyModule()
        {
            this.Get["/"] = _ => this.View["swagger"];
        }
    }
}
