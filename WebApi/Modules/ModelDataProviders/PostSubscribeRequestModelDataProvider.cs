﻿using Nancy.Swagger;
using Nancy.Swagger.Services;
using Service.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Modules.ModelDataProviders
{
    public class PostSubscribeRequestModelDataProvider : ISwaggerModelDataProvider
    {
        public SwaggerModelData GetModelData()
        {
            return SwaggerModelData.ForType<PostSubscribeRequest>(with =>
            {
                with.Description("Post a new subscription....");

                with.Property(x => x.PublicationId)
                    .Description("PublicationId....")
                    .Required(true);

                with.Property(x => x.SubscriptionId)
                    .Description("SubscriptionId ....")
                    .Required(true);

                with.Property(x => x.PaymentId)
                    .Description("PaymentId.....")
                    .Required(true);

                with.Property(x => x.PromotionalCode)
                   .Description("PromotionalCode.....");
            });
        }
    }
}