﻿using Common.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Infrastructure.Services.SubscriptionsRepositorie
{
    public interface ISubscriptionsRepositorie
    {
        SubscriptionsTypeModel GetSubscriptions(int publicationID);
        SubscriptionTypeModel GetSubscription(int publicationID, int? subscriptionTypeID);
    }
}
