﻿using Common.Infrastructure.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Common.Infrastructure.Services.SubscriptionsRepositorie
{
    public class SubscriptionsRepositorie : ISubscriptionsRepositorie
    {
        public SubscriptionsTypeModel GetSubscriptions(int publicationID)
        {
            var subscriptionsModel = new SubscriptionsTypeModel();
            subscriptionsModel.SubscriptionsType = LoadSubscriptions(publicationID).ToList();

            return subscriptionsModel;
        }

        public SubscriptionTypeModel GetSubscription(int publicationID, int? subscriptionTypeID)
        {
            var subscriptionType = new SubscriptionTypeModel();
            subscriptionType = LoadSubscriptions(publicationID, subscriptionTypeID).FirstOrDefault();

            return subscriptionType;
        }

        private IEnumerable<SubscriptionTypeModel> LoadSubscriptions(int publicationID, int? subscriptionTypeID = null)
        {
            
            var subscriptionsList = new List<SubscriptionTypeModel>();

            //using (Database database = new Database("CofinaServices"))
            //{
            //    ArrayList parameters = new ArrayList();

            //    if (subscriptionTypeID != null)
            //    {
            //        parameters.Add(new Database.DBParams("id", SqlDbType.Int, subscriptionTypeID, null));
            //    }

            //    parameters.Add(new Database.DBParams("publication", SqlDbType.Int, publicationID, null));
            //    IDataReader reader = null;
            //    database.ExecuteReader(CommandType.StoredProcedure, "GetSubscriptions", parameters, out reader);

            //    if (reader != null)
            //    {
            //        while (reader.Read())
            //        {
            //            var subscriptionType = new SubscriptionTypeModel();
            //            subscriptionType.SubscriptionTypeID = (int)reader["Content_ID"];
            //            subscriptionType.Description = (string)reader["Descricao"];
            //            subscriptionType.Name = (string)reader["Title"];
            //            subscriptionType.SelfRenewable = reader["AutoRenovacao"] != DBNull.Value && reader["AutoRenovacao"].ToString() == "1";
            //            subscriptionType.Price = reader["Preco"] != DBNull.Value ? reader["Preco"].ToString() : "0";
            //            if (reader["DuracaoDias"] != DBNull.Value)
            //            {
            //                int duration = 0;
            //                if (int.TryParse((string)reader["DuracaoDias"], out duration))
            //                {
            //                    subscriptionType.DurationInDays = duration;
            //                }
            //            }

            //            subscriptionType.SapIdentifier = reader["SapIdentificador"] != DBNull.Value ? reader["SapIdentificador"].ToString() : string.Empty;
            //            subscriptionType.BestPrice = reader["MelhorPreco"] != DBNull.Value && reader["MelhorPreco"].ToString() == "1";
            //            subscriptionType.Resumo = reader["Resumo"] != DBNull.Value ? reader["Resumo"].ToString() : string.Empty;

            //            subscriptionsList.Add(subscriptionType);
            //        }

            //    }
            //}

            return subscriptionsList;
        }

    }
}