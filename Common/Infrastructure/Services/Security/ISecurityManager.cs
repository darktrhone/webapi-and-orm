﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Infrastructure.Services.Security
{
    public interface ISecurityManager
    {
        string GenerateToken(string username, int userId, string apiKey, long ticks);
        string GetHashedApiKey(string apiKey);
        bool IsTokenValid(string token);
    }
}
