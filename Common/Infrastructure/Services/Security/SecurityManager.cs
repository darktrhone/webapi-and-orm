﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Common.Infrastructure.Services;
using Cofina.Logger;
using System.Configuration;

namespace Common.Infrastructure.Services.Security
{
    public class SecurityManager : ISecurityManager
    {
        private const string _alg = "HmacSHA256";
        private const string _salt = "83HdPl7OHrR75IhDT4lF"; // Generated at https://www.random.org/strings

        /// <summary>
        /// Criar token para ser utilizada no serviço de subscrição.
        /// A validade da token é de 10 minutos
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="apiKey"></param>
        /// <param name="ticks"></param>
        /// <returns></returns>
        public string GenerateToken(string username, int userId, string apiKey, long ticks)
        {
            try
            {
                string hash = string.Join(":", new string[] { username, ticks.ToString() });
                string hashLeft = "";
                string hashRight = "";
                using (HMAC hmac = HMACSHA256.Create(_alg))
                {
                    hmac.Key = Encoding.UTF8.GetBytes(GetHashedApiKey(apiKey));
                    hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                    hashLeft = Convert.ToBase64String(hmac.Hash);
                    hashRight = string.Join(":", new string[] { username, userId.ToString(), ticks.ToString() });
                }

                return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
            }
            catch (Exception error)
            {
                CofinaLogList.Instance.Add(error);
                CofinaLogList.Instance.Log();
                return string.Empty;
            }           
            
        }

        /// <summary>
        /// Gerar Hash da apiKey
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns></returns>
        public string GetHashedApiKey(string apiKey)
        {
            try
            {
                string key = string.Join(":", new string[] { apiKey, _salt });
                using (HMAC hmac = HMACSHA256.Create(_alg))
                {
                    // Hash the key.
                    hmac.Key = Encoding.UTF8.GetBytes(_salt);
                    hmac.ComputeHash(Encoding.UTF8.GetBytes(key));
                    return Convert.ToBase64String(hmac.Hash);
                }
            }
            catch (Exception error)
            {
                CofinaLogList.Instance.Add(error);
                CofinaLogList.Instance.Log();
                return string.Empty;
            }
            
        }        

        private const int _expirationMinutes = 10;
        /// <summary>
        /// Validar token.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsTokenValid(string token)
        {
            bool result = false;
            try
            {
                // Base64 decode the string, obtaining the token:username:userid:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 4)
                {
                    // Get the hash message, username, userid and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    string userId = parts[2];
                    long ticks = long.Parse(parts[3]);
                    DateTime timeStamp = new DateTime(ticks);

                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        var repositorie = new UsersRepositorie();
                        var user = repositorie.GetUserById(int.Parse(userId));

                        if (user != null && username == user.Name)
                        {
                            string apiKey = ConfigurationManager.AppSettings["CreateSubscriptionApiKey"];
                            
                            // Hash the message with the key to generate a token.
                            string computedToken = GenerateToken(user.Name, user.UserID,  apiKey, ticks);
                            // Compare the computed token with the one supplied and ensure they match.
                            result = (token == computedToken);
                        }
                    }
                }
            }
            catch (Exception error)
            {
                CofinaLogList.Instance.Add(error);
                CofinaLogList.Instance.Log();
                return false;
            }

            return result;
        }
    }
}
