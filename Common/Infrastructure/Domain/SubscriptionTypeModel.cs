﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Common.Infrastructure.Domain
{
    public class SubscriptionTypeModel
    {
        [Required(ErrorMessage = "* Tem que selecionar um tipo de assinatura.")]
        public int SubscriptionTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        private string _Price;
        public string Price
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_Price))
                {
                    _Price = "0";
                }
                return _Price;
            }
            set { _Price = value; }
        }

        public bool SelfRenewable { get; set; }
        public int DurationInDays { get; set; }
        public string SapIdentifier { get; set; }
        public bool BestPrice { get; set; }
        public string Resumo { get; set; }
    }
}