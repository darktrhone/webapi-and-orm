﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Common.Infrastructure.Domain
{
    public class SubscriptionsTypeModel
    {
        [Required(ErrorMessage = "* Tem que selecionar uma assinatura.")]
        public List<SubscriptionTypeModel> SubscriptionsType { get; set; }
    }
}